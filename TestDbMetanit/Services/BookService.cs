﻿using System.Linq;
using System.Threading.Tasks;
using TestDbMetanit.Repositories.Interfaces;
using TestDbMetanit.Services.Interfaces;
using TestDbMetanit.Views;

namespace TestDbMetanit.Services
{
    public class BookService : IBookService
    {
        private IBookRepository _bookRepository;
        private IBookAuthorRepository _bookAuthorRepository;
        private IAuthorRepository _authorRepository;
        public BookService(IBookRepository bookRepository, IBookAuthorRepository bookAuthorRepository, IAuthorRepository authorRepository)
        {
            _bookRepository = bookRepository;
            _bookAuthorRepository = bookAuthorRepository;
            _authorRepository = authorRepository;
        }

        public async Task Add(GetBooksView view)
        {
            throw new System.NotImplementedException();
        }

        public async Task<GetBooksView> GetBooks()
        {
            var bookView = new GetBooksView();

            var books = await _bookRepository.Get();

            bookView.Books.AddRange(
               books.Select(x => new BookGetBooksViewItem
               {
                   Id = x.Id,
                   Title = x.Title,
                   Authors = books.Where(y => y.BookAuthor.AuthorId == x.Id).Select(ob => new AuthorGetBooksViewItem
                   {
                       Id = ob.BookAuthor.Author.Id,
                       FirstName = ob.BookAuthor.Author.FirstName,
                       LastName = ob.BookAuthor.Author.LastName
                   }).ToList()
               }).ToList());

            return bookView;
        }
    }
}
