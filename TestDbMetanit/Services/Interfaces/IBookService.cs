﻿using System.Threading.Tasks;
using TestDbMetanit.Views;

namespace TestDbMetanit.Services.Interfaces
{
    public interface IBookService
    {
        Task<GetBooksView> GetBooks();
        Task Add(GetBooksView view);
    }
}
