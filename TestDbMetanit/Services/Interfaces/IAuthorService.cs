﻿using System.Threading.Tasks;
using TestDbMetanit.Views.AuthorViews;

namespace TestDbMetanit.Services.Interfaces
{
    public interface IAuthorService
    {
        Task Add(CreateAuthorView view);
        Task<GetAuthorsView> Get();
        Task<GetAuthorView> Get(int id);
        Task Update(UpdateAuthorView view);
        Task Delete(int id);

    }
}
