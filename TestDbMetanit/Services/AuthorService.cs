﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TestDbMetanit.Entity;
using TestDbMetanit.Repositories.Interfaces;
using TestDbMetanit.Services.Interfaces;
using TestDbMetanit.Views;
using TestDbMetanit.Views.AuthorViews;

namespace TestDbMetanit.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;
        public AuthorService(IAuthorRepository authorRepository)
        {
            _authorRepository = authorRepository;
        }
        public async Task Add(CreateAuthorView view)
        {
            var author = new Author
            {
                FirstName = view.FirstName,
                LastName = view.LastName
            };
            await _authorRepository.Insert(author);
        }

        public async Task Delete(int id)
        {
            await _authorRepository.Remove(id);
        }

        public async Task<GetAuthorsView> Get()
        {
            var authorView = new GetAuthorsView();

            var authors = await _authorRepository.Get();

            authorView.Authors.AddRange(
                authors.Select(x => new AuthorGetAuthorsViewItem
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    CreationDate = x.CreationDate,
                    UpdationDate = x.UpdationDate
                }).ToList());

            return authorView;
        }

        public async Task<GetAuthorView> Get(int id)
        {
            var author = await _authorRepository.GetAuthorIfExist(id);
            if (author != null)
            {
                var authorView = new GetAuthorView
                {
                    Id = author.Id,
                    FirstName = author.FirstName,
                    LastName = author.LastName,
                    CreationDate = author.CreationDate,
                    UpdationDate = author.UpdationDate
                };
                return authorView;
            }
            return null;
        }

        public async Task Update(UpdateAuthorView view)
        {
            var author = await _authorRepository.Get(view.Id);

            author.FirstName = view.FirstName;
            author.LastName = view.LastName;
            author.UpdationDate = DateTime.Now;

            await _authorRepository.Update(author);
        }
    }
}
