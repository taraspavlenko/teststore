﻿using TestDbMetanit.Entities.BaseEntities;

namespace TestDbMetanit.Entity
{
    public class Author : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual BookAuthor BookAuthor { get; set; }
    }
}