﻿using TestDbMetanit.Entities;
using TestDbMetanit.Entities.BaseEntities;

namespace TestDbMetanit.Entity
{
    public class Book : BaseEntity
    {
        public string Title { get; set; }

        public virtual BookAuthor BookAuthor { get; set; }
    }
}
