﻿using System;

namespace TestDbMetanit.Entities.BaseEntities
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            CreationDate = DateTime.Now;
        }
        public int Id { get; set; }
        public DateTime CreationDate { get; private set; }
        public DateTime? UpdationDate { get; set; }
    }
}
