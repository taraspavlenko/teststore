﻿using System.Collections.Generic;
using TestDbMetanit.Entities.BaseEntities;
using TestDbMetanit.Entity;

namespace TestDbMetanit.Entities
{
    public class Order : BaseEntity
    {
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Total { get; set; }

        public int BookId { get; set; }
        public virtual Book Book { get; set; }
    }
}
