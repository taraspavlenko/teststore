﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestDbMetanit.Repositories.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task Insert(TEntity item);
        Task InsertRange(List<TEntity> items);
        Task<IEnumerable<TEntity>> Get();
        Task<TEntity> Get(int key);
        Task Remove(int id);
        Task Update(TEntity item);
    }
}
