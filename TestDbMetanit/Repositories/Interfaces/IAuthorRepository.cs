﻿using System.Threading.Tasks;
using TestDbMetanit.Entity;

namespace TestDbMetanit.Repositories.Interfaces
{
    public interface IAuthorRepository:IRepository<Author>
    {
        Task<Author> GetAuthorIfExist(int id);
    }
}
