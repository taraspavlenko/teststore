﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestDbMetanit.Entity;

namespace TestDbMetanit.Repositories.Interfaces
{
    public interface IBookAuthorRepository : IRepository<BookAuthor>
    {

    }
}
