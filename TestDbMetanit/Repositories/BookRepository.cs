﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestDbMetanit.Context;
using TestDbMetanit.Entity;
using TestDbMetanit.Repositories.Interfaces;

namespace TestDbMetanit.Repositories
{
    class BookRepository : IBookRepository
    {
        private ApplicationContext _dbContext;

        public BookRepository(ApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Book>> Get()
        {
            var books = await _dbContext.Books
               .ToListAsync();
            return books;
        }

        public Task<Book> Get(int key)
        {
            throw new NotImplementedException();
        }

        public Task Insert(Book item)
        {
            throw new NotImplementedException();
        }

        public Task InsertRange(List<Book> items)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public Task Update(Book item)
        {
            throw new NotImplementedException();
        }
    }
}
