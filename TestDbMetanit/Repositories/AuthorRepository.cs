﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestDbMetanit.Context;
using TestDbMetanit.Entity;
using TestDbMetanit.Repositories.Interfaces;

namespace TestDbMetanit.Repositories
{
    public class AuthorRepository : IAuthorRepository, IRepository<Author>
    {
        private ApplicationContext _dbContext;
        public AuthorRepository(ApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IEnumerable<Author>> Get()
        {
            var authors = await _dbContext.Authors
                .ToListAsync();
            return authors;
        }
        public async Task Insert(Author item)
        {
            if (!IsExist(item))
            {
                await _dbContext.AddAsync(item);
                await _dbContext.SaveChangesAsync();
            }
        }
        public async Task InsertRange(List<Author> items)
        {
            await _dbContext.AddRangeAsync(items);
            await _dbContext.SaveChangesAsync();
        }
        public async Task Remove(int id)
        {
            var itemToRemove = await GetAuthorIfExist(id);
            if (itemToRemove != null)
            {
                _dbContext.Authors.Remove(itemToRemove);
                await _dbContext.SaveChangesAsync();
            }
        }
        public async Task<Author> Get(int id)
        {
            var author = await _dbContext.Authors
               .Where(e => e.Id == id)
               .SingleOrDefaultAsync();
            return author;
        }
        public async Task Update(Author author)
        {
            _dbContext.Authors.Update(author);
            await _dbContext.SaveChangesAsync();
        }
        public async Task<Author> GetAuthorIfExist(int id)
        {
            var author = await _dbContext.Authors.
                Where(x => x.Id == id).FirstOrDefaultAsync();

            return author;
        }
        public bool IsExist(Author author)
        {
            bool flag = _dbContext.Authors.
                Where(x => x.FirstName == author.FirstName && x.LastName == author.LastName)
                .Any();
            if (flag)
            {
                return true;
            }
            return false;
        }
        private bool IsExist(int id)
        {
            bool flag = _dbContext.Authors.
                Where(x => x.Id == id)
                .Any();
            if (flag)
            {
                return true;
            }
            return false;
        }
    }
}
