﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestDbMetanit.Context;
using TestDbMetanit.Entity;
using TestDbMetanit.Repositories.Interfaces;

namespace TestDbMetanit.Repositories
{
    public class BookAuthorRepository : IBookAuthorRepository
    {
        private ApplicationContext _dbContext;
        public BookAuthorRepository(ApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IEnumerable<BookAuthor>> Get()
        {
            var bookAuthors = await _dbContext.BookAuthors
                .ToListAsync();
            return bookAuthors;
        }
        public Task<BookAuthor> Get(int key)
        {
            throw new System.NotImplementedException();
        }

        public Task Insert(BookAuthor item)
        {
            throw new System.NotImplementedException();
        }

        public Task InsertRange(List<BookAuthor> items)
        {
            throw new System.NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task Update(BookAuthor item)
        {
            throw new System.NotImplementedException();
        }
    }
}
