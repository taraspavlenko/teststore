﻿using System.Collections.Generic;

namespace TestDbMetanit.Views
{
    public class GetBooksView
    {
        public GetBooksView()
        {
            Books = new List<BookGetBooksViewItem>();
        }
        public List<BookGetBooksViewItem> Books{ get; set; }
    }

    public class BookGetBooksViewItem
    {
        public BookGetBooksViewItem()
        {
            Authors = new List<AuthorGetBooksViewItem>();   
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public List<AuthorGetBooksViewItem> Authors { get; set; }
    }

    public class AuthorGetBooksViewItem
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
