﻿using System;

namespace TestDbMetanit.Views.AuthorViews
{
    public class GetAuthorView
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? UpdationDate { get; set; }
    }
}
