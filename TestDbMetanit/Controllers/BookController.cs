﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TestDbMetanit.Services.Interfaces;
using TestDbMetanit.Views;

namespace TestDbMetanit.Controllers
{
    [Route("api/[controller]/[action]")]
    public class BookController : Controller
    {
        private IBookService _service;
        public BookController(IBookService service)
        {
            _service = service;
        }
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var view = new GetBooksView();
            view = await _service.GetBooks();
            return Ok(view);
        }
        //[HttpGet("{id}")]
        //public async Task<IActionResult> Get(int id)
        //{
        //    var view = new GetBooksView();
        //    view = await _service.GetBook()
        //}
        [HttpPost]
        public async Task<IActionResult> Greate([FromBody] GetBooksView view)
        {
            await _service.Add(view);
            return Ok();
        }

    }
}
