﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TestDbMetanit.Services.Interfaces;
using TestDbMetanit.Views.AuthorViews;

namespace TestDbMetanit.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AuthorController : Controller
    {
        private IAuthorService _authorService;
        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateAuthorView viewModel)
        {
            if (viewModel == null)
            {
                return BadRequest();
            }
            await _authorService.Add(viewModel);
            return Ok(viewModel);

        }
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var view = await _authorService.Get();
            return Ok(view);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var view = await _authorService.Get(id);
            if (view != null)
            {
                return Ok(view);
            }
            return NotFound();
        }
        [HttpPost("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _authorService.Delete(id);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Update([FromBody] UpdateAuthorView view)
        {
            if (view != null)
            {
                await _authorService.Update(view);
                return Ok(view);
            }
            return BadRequest();
        }
    }
}
